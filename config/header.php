<?php
	define("BASE_URL", "http://localhost/todoo/");
	//define("BASE_URL", "http://192.168.43.36/todoo/");
?>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title>LOGIN TODO</title>
		<!--link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css"-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="assets/vendor/mdl/material.min.css">
		<link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">
		<link rel="stylesheet" href="assets/css/custom.css">
		
		
		<script src="assets/vendor/jquery/jquery-3.2.1.min.js"></script>
		<!--script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script-->
		<script src="assets/vendor/bootstrap/js/popper.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script defer src="assets/vendor/mdl/material.min.js"></script>
		<script defer src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
		
	</head>
	<body>