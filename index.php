<?php

require "config/connection.php";
require "config/header.php";
session_start();

if(!$_SESSION['loggedin']){
	header("location:login.php");
}

if(isset($_GET['logout'])){
	session_destroy();
	header("location:login.php");
}

?>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
	<header class="mdl-layout__header">
		<div class="mdl-layout__header-row">
			<div class="mLogo"></div>
			<div class="mdl-layout-spacer"></div>
			<a id="profile" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent person" data-bb-example-key="prompt-textarea"><span>Profile</span><i class="material-icons">person</i></a>
			<a href="?logout=true" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent logout"><span>Logout</span> <i class="material-icons">logout</i></a>
		</div>
	</header>
	
	<main class="mdl-layout__content">
		<div class="page-content">
			<form id="todoForm">
				<div class="md-form input-group mb-3">
					<input type="text" class="form-control" placeholder="Your Text Todo" name="todo_text" required>
					<div class="input-group-append">
						<button class="btn btn-md btn-secondary m-0 px-3 waves-effect waves-light savingTodo" type="submit" ><i class="material-icons">save</i> <span>Simpan</span> </button>
					</div>
					<input type="hidden" name="user_id" value="<?=$_SESSION['user_id'];?>">
				</div>
			</form>
			<hr>
			<div class="resTodo"></div>
		</div>
		
	</main>
</div>

<div class="mdl-modal" style="display:none">
	<div class="mdl-modal-container">
		<form id="profileForm">
			<div class="mdl-modal-header">
				My Profile
				<div class="user__root">
					<span></span>
				</div>
			</div>
			<div class="mdl-modal-content">
				<div id="profile-status"></div>
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input class="mdl-textfield__input" type="text" name="p_name" required>
						<label class="mdl-textfield__label" >Nama Lengkap</label>
					</div>
				</div>
				<hr>
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="password" name="p_opwd" >
						<label class="mdl-textfield__label" >Password Lama</label>
					</div>
				</div>
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="password" name="p_pwd" >
						<label class="mdl-textfield__label" >Password Baru</label>
					</div>
				</div>
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input class="mdl-textfield__input" type="password" name="p_cpwd" >
						<label class="mdl-textfield__label" >Konfirmasi Password Baru</label>
					</div>
				</div>
			
			</div>
			<div class="mdl-modal-footer">
				<button id="btnCancel" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent mute" type="button"><i class="material-icons">close</i> Batal</button>
				<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="material-icons">save</i> Simpan</button>
			</div>
		</form>
	</div>
</div>

<script>
	
	$("#todoForm").submit(function(event){
		event.preventDefault();
		$.ajax({
			type:"post",
			url:"response.php?add",
			data:new FormData(this),
			contentType: false,      
			cache: false,            
			processData:false,
			success:function(r){
				if(r){
					todoLoad();
					$("input[name=todo_text]").val("");
					$("input[name=todo_text]").focus();
				}
			}
			
		});
		
	});
	
	function todoLoad(){
		$.ajax({
			type:"post",
			url:"response.php?uid=<?=$_SESSION['user_id'];?>&list",
			success:function(r){
				$(".resTodo").html(r);
			}
		});
	}
	
	function todoDel(id){
		
		$.ajax({
			type:"post",
			url:"response.php?id="+id+"&delete",
			success:function(r){
				if(r){
					todoLoad();
				}
			}
		});
	}
	todoLoad();
	$("input[name=todo_text]").focus();
	$("#profile").on("click", function(){
		$(".mdl-modal").fadeIn();
		$.ajax({
			type:"post",
			url:"response.php?id=<?=$_SESSION['user_id'];?>&getuser",
			dataType:"json",
			success:function(r){
				$("input[name=p_name]").val(r.name);
				$("input[name=p_name]").focus();
				$(".user__root span").text(r.user);
			}
		})
	});
	$("#profileForm").submit(function(event){
		event.preventDefault();
		$.ajax({
			type:"post",
			url:"response.php?id=<?=$_SESSION['user_id'];?>&upgrade",
			data:new FormData(this),
			dataType:"json",
			contentType:false,
			cache:false,
			processData:false,
			success:function(e){
				if(e.status){
					$(".mdl-modal").fadeOut();
					swal("Sukses!", "Profil mu telah disimpan!", "success");
				}else{
					$("#profile-status").html(e.content);
					$("input[type=password]").val("");
				}
			}
		});
	});
	$("#btnCancel").on("click", function(){
		$(".mdl-modal").fadeOut();
	});
</script>



	