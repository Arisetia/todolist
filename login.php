<?php require "config/header.php"; ?>
<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--8-col">
		<div class="ctr-bg">
			<div class="login-bg"></div>
		</div>
	</div>
	<div class="mdl-cell mdl-cell--4-col">
		
		<div class="mtl-wrapper">
			<div id="prog" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
			<div class="mlt-content">
				<img src="assets/images/logo_dark.png">
				<hr>
				<div id="status"></div>
				
                <form id="loginForm">
					<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" name="username" required>
							<label class="mdl-textfield__label" >User name</label>
						</div>
						<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="password" name="password" required>
							<label class="mdl-textfield__label">Password</label>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">LOGIN</button>
							<div id="logStatus"></div>
							<hr>
							<p>Belum punya akun? <a onclick="getForm()">Daftar Sekarang</a></p>
						</div>
                    </div>
                </form>
				<form id="registerForm">
                    <div class="mdl-grid">
						<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input type="text" name="r_name" class="mdl-textfield__input" required>
                            <label class="mdl-textfield__label">Nama Lengkap</label>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" name="r_uname" required>
							<label class="mdl-textfield__label" >Username</label>
						</div>
						<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="password" name="r_pwd" required>
							<label class="mdl-textfield__label" >Set Password</label>
						</div>
						<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="password" name="r_cpwd" required>
							<label class="mdl-textfield__label" >Konfirmasi Password</label>
						</div>
                        <div class="mdl-cell mdl-cell--12-col">           
							<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" >REGISTER</button>
							<div id="regStatus"></div>
							<hr>
							<p>Sudah punya akun? <a onclick="getForm()">Login Sekarang</a></p>
							
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>
</div> 
<script>
	function getForm(){ 
		$("#registerForm, #loginForm").toggle(); 
	}
	$("#registerForm").on("submit",function(event){ 
		event.preventDefault();
		$.ajax({ 
			type:"post", 
			url:"response.php?register=true", 
			data:$(this).serialize(),
			dataType:"json", 
			cache:false, 
			contenType:false, 
			processData:false, 
			success:function(r){ 
				if(r.status){ 
					$("#status").html(r.content); getForm(); 
				}else{ 
					$("#regStatus").html(r.content); 
					if(r.code == 1){
						$("input[name=r_uname], input[name=r_pwd], input[name=r_cpwd]").val("");
					}else{
						$("input[name=r_pwd], input[name=r_cpwd]").val("");
					}
				}
			}
		});
	});
	
	$("#loginForm").on("submit",function(event){
		event.preventDefault();
		$.ajax({
			type:"post",
			url:"response.php?login=true",
			data:$(this).serialize(),
			dataType:"json",
			cache:false,
			contenType:false,
			processData:false,
			success:function(r){
				$("#prog").fadeIn();
				setTimeout(function(){
					if(r.status){
						window.location.href="<?=BASE_URL;?>";
					}else{
						$("#prog").fadeOut();
						$("#logStatus").html(r.content);
						$("input[name=username], input[name=password]").val("");
					}
				}, 2000);
			}
		});
	});
	$("#registerForm, #prog").hide();
</script>
		

